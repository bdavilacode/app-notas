
let boton = document.getElementById('calcula');

function calcular(){
    let n1 = parseInt(document.getElementById('numero1').value);
    let n2 = parseInt(document.getElementById('numero2').value);
    let n3 = parseInt(document.getElementById('numero3').value);
    let promedio = (n1 + n2 + n3)/3;
    //console.log(Math.round(promedio));

    document.getElementById('promedio').value = Math.round(promedio);
    if (promedio >= 13){
        document.getElementById('condicion').value = 'Aprobado';
    } else {
        document.getElementById('condicion').value = 'Desaprobado';
    }
}

boton.addEventListener('click', calcular);


function limpiar(){
    document.getElementsByTagName('input').value = '';

}

